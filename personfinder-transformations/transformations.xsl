<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>We've found the person!
                    <xsl:value-of select="/game-site" />
                </h1>
                <table>
                    <tr>
                        <td>
                            <h2>Show your persons (older than 18, rating larger then 5):</h2>
                        </td>
                        <td>
                            <xsl:apply-templates select="//person[@age &gt;= 18][@rating &gt;= 5]" />
                        </td>
                    </tr>
                </table>
                <table>
                    <td>
                        <h1>
                            More persons:
                        </h1>
                    </td>
                    <xsl:apply-templates select="//person"/>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="person">
            <tr>
                <td><xsl:value-of select="@name"/></td>
                <td><xsl:value-of select="@gender"/></td>
                <td><xsl:value-of select="@rating"/></td>
                <td><xsl:value-of select="@age"/></td>
                <td><xsl:value-of select="@aim"/></td>
                <td><xsl:value-of select="@status"/></td>
                <td><xsl:value-of select="@height"/></td>
                <td><xsl:value-of select="@country"/></td>
                <td><xsl:value-of select="@city"/></td>
            </tr>
    </xsl:template>
</xsl:stylesheet>
